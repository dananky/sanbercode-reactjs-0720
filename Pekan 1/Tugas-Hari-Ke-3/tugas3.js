//Soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";
var jawabSoal1 = kataPertama + " " + kataKedua.charAt(0).toUpperCase() + kataKedua.substr(1) + " " + kataKetiga + " " + kataKeempat.toUpperCase();

console.log(jawabSoal1);

//Soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";
var jawabSoal2 = parseInt(kataPertama) + parseInt(kataKedua) + parseInt(kataKetiga) + parseInt(kataKeempat);

console.log(jawabSoal2);

//Soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); 
var kataKetiga = kalimat.substring(15, 18); 
var kataKeempat = kalimat.substring(19, 24); 
var kataKelima = kalimat.substring(25, 31); 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

//Soal 4
var nilai;

nilai = 79;

if (nilai >= 80) {
    console.log("Indeks A");
} else if (nilai < 80 && nilai >= 70) {
    console.log("Indeks B");
} else if (nilai < 70 && nilai >= 60) {
    console.log("Indeks C");
} else if (nilai < 60 && nilai >= 50) {
    console.log("Indeks D");
} else if (nilai < 50) {
    console.log("Indeks E");
}

//Soal 5
var tanggal = 23;
var bulan = 3;
var tahun = 1997;

switch (bulan) {
    case 1:
        bulan = "Januari";
        break;
    case 2:
        bulan = "Februari";
        break;
    case 3:
        bulan = "Maret";
        break;
    case 4:
        bulan = "April";
        break;
    case 5:
        bulan = "Mei";
        break;
    case 6:
        bulan = "Juni";
        break;
    case 7:
        bulan = "Juli";
        break;
    case 8:
        bulan = "Agustus";
        break;
    case 9:
        bulan = "September";
        break;
    case 10:
        bulan = "Oktober";
        break;
    case 11:
        bulan = "November";
        break;
    case 12:
        bulan = "Desember";
        break;
    default:
        bulan = 0;
        console.log("Bulan tidak ada!");
        break;
}

var jawabSoal5 = tanggal + " " + bulan + " " + tahun;

console.log(jawabSoal5);