//Soal 1
var i = 2;

console.log("LOOPING PERTAMA");
while (i <= 20) {
  console.log(i, "- I love coding");
  i += 2;
}
console.log("LOOPING KEDUA");
while (i >= 4) {
  i -= 2;
  console.log(i, "- I will become a frontend developer");
}

//Soal 2
var tampil;
for (var i = 1; i <= 20; i++) {
  if (i % 2 === 1) {
    if (i % 3 === 0) {
      tampil = "I Love Coding";
    } else {
      tampil = "Santai";
    }
  } else if (i % 2 === 0) {
    tampil = "Berkualitas";
  }
  console.log(i, "-", tampil);
}

//Soal 3
var hashtag = "";
for (var i = 1; i <= 7; i++) {
  hashtag += "#";
  console.log(hashtag);
}

//Soal 4
var kalimat = "saya sangat senang belajar javascript";
var hasil = kalimat.split(" ");
console.log(hasil);

//Soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort();

for (let i = 0; i < daftarBuah.length; i++) {
    console.log(daftarBuah[i]);
    
}
