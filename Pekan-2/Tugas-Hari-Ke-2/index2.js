var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

function readBook() {
  readBooksPromise(10000, books[0])
    .then(function (res) {
      readBooksPromise(res, books[1])
        .then(function (res) {
          readBooksPromise(res, books[2])
            .then(function (res) {
              console.log("Buku habis!");
            })
            .catch(function (error) {
              console.log("Waktu tidak cukup!");
            });
        })
        .catch(function (error) {
          console.log("Waktu tidak cukup!");
        });
    })
    .catch(function (error) {
      console.log("Waktu tidak cukup!");
    });
}

readBook();
