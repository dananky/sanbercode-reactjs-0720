var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
  { name: "komik", timeSpent: 1000 },
];

readBooks(10000, books[0], function (timeLeft) {
  readBooks(timeLeft, books[1], function (timeLeft) {
    readBooks(timeLeft, books[2], function (timeLeft) {
      readBooks(timeLeft, books[3], function (timeLeft) {
        console.log("Waktu tidak cukup!");
      });
    });
  });
});
