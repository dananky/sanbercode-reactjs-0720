//Soal 1
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku", 1992];
var objectDaftarPeserta = {
  nama: arrayDaftarPeserta[0],
  "jenis kelamin": arrayDaftarPeserta[1],
  hobi: arrayDaftarPeserta[2],
  "tahun lahir": arrayDaftarPeserta[3],
};

//Soal 2
var dataBuah = [
  {
    nama: "strawberry",
    warna: "merah",
    "ada bijinya": "tidak",
    harga: 9000,
  },
  {
    nama: "jeruk",
    warna: "oranye",
    "ada bijinya": "ada",
    harga: 8000,
  },
  {
    nama: "Semangka",
    warna: "Hijau & Merah",
    "ada bijinya": "ada",
    harga: 10000,
  },
  {
    nama: "Pisang",
    warna: "Kuning",
    "ada bijinya": "tidak",
    harga: 5000,
  },
];

console.log(dataBuah[0]);

//Soal 3
var dataFilm = [];

function tambahDataFilm(nama, durasi, genre, tahun) {
  var objectFilm = {
    nama: nama,
    durasi: durasi,
    genre: genre,
    tahun: tahun,
  };

  return dataFilm.push(objectFilm);
}

tambahDataFilm("IT", "2 jam", "Horror, Thriller", "2017");
console.log(dataFilm);

//Soal 4
class Animal {
  constructor(name) {
    this._name = name;
    this._legs = 4;
    this._cold_blooded = false;
  }

  get name() {
    return this._name;
  }

  get legs() {
    return this._legs;
  }

  get cold_blooded() {
    return this._cold_blooded;
  }
}

var sheep = new Animal("shaun");

console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);

class Ape extends Animal {
  constructor(_name, _cold_blooded) {
    super(_name, _cold_blooded);
  }

  yell() {
    console.log("Auooo");
  }
}

class Frog extends Animal {
  constructor(_name, _legs, _cold_blooded) {
    super(_name, _legs, _cold_blooded);
  }

  jump() {
    console.log("hop hop");
  }
}

var sungokong = new Ape("kera sakti");
sungokong.yell();

var kodok = new Frog("buduk");
kodok.jump();

//Soal 5
class Clock {
  constructor({ template }) {
    var timer;
    function render() {
      var date = new Date();

      var hours = date.getHours();
      if (hours < 10) hours = "0" + hours;

      var mins = date.getMinutes();
      if (mins < 10) mins = "0" + mins;

      var secs = date.getSeconds();
      if (secs < 10) secs = "0" + secs;

      var output = template
        .replace("h", hours)
        .replace("m", mins)
        .replace("s", secs);

      console.log(output);
    }

    this.stop = function () {
      clearInterval(timer);
    };

    this.start = function () {
      render();
      this.timer = setInterval(render, 1000);
    };
  }
}

var clock = new Clock({ template: "h:m:s" });
clock.start();
