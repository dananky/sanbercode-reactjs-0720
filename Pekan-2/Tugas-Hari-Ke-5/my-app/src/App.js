import React from "react";
import "./App.css";

function App() {
  return (
    <div className="App">
      <div className="content">
        <h1>Form Pembelian Buah</h1>
        <form action="#">
          <table className="form-buah">
            <tr>
              <th>Nama Pelanggan</th>
              <td>
                <input type="text" />
              </td>
            </tr>
            <tr>
              <th style={{ verticalAlign: "bottom" }}>Daftar Item</th>
              <td>
                <input type="checkbox" name="buah" id="semangka" />
                Semangka <br />
                <input type="checkbox" name="buah" id="jeruk" />
                Jeruk <br />
                <input type="checkbox" name="buah" id="nanas" />
                Nanas <br />
                <input type="checkbox" name="buah" id="salak" />
                Salak <br />
                <input type="checkbox" name="buah" id="anggur" />
                Anggur <br />
              </td>
            </tr>
            <tr>
              <td colSpan="2">
                <button className="btn-buah">Kirim</button>
              </td>
            </tr>
          </table>
        </form>
      </div>
    </div>
  );
}

export default App;
