//Soal 1
console.log("===== Soal 1 =====");
const luasLingkaran = (r) => {
  const phi = 3.14;
  let jariJari = r;

  return phi * Math.pow(jariJari, 2);
};

const kelilingLingkaran = (r) => {
  const phi = 3.14;
  let jariJari = r;

  return 2 * phi * jariJari;
};

console.log(luasLingkaran(1));
console.log(kelilingLingkaran(1));

//Soal 2
console.log();
console.log("===== Soal 2 =====");
let kalimat = "";

let kata1 = "saya";
let kata2 = "adalah";
let kata3 = "seorang";
let kata4 = "frontend";
let kata5 = "developer";

const gabungKata = (kata1, kata2, kata3, kata4, kata5) => {
  kalimat = `${kata1} ${kata2} ${kata3} ${kata4} ${kata5}`;
  return kalimat;
};

console.log(gabungKata(kata1, kata2, kata3, kata4, kata5));

//Soal 3
console.log();
console.log("===== Soal 3 =====");
class Book {
  constructor(name, totalPage, price) {
    this._name = name;
    this._totalPage = totalPage;
    this._price = price;
  }
}

class Komik extends Book {
  constructor(name, totalPage, price, isColorful) {
    super(name, totalPage, price);
    this._isColorful = isColorful;
  }
}

let buku = new Book("Buku Bukuan", 100, 30000);
let komik = new Komik("Komik Komikan", 200, 60000, true);

console.log(buku);
console.log(komik);
